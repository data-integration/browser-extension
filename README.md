# Repommender

This repository contains a chrome extension providing recommendations for repositories hosted on GitHub, GitLab and Bitbucket.

## Installation

Download the latest realse, unpack the zip file, activate Chrome's [developer mode](https://developer.chrome.com/extensions/faq#faq-dev-01) and install the extension as "unpacked extension".
