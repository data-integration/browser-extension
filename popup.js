
let platformId = null
let userName = ''
let userId = ''
let approach= 'cf'
let userUrl = ''

let topics = []
const ratings = []
const userRatings = []
let sum = 0
let cold = false

getURLData();

function getURLData() {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
        let tab = tabs[0];
        let url = new URL(tab.url);
        let hostname = url.hostname;
        let pathname = url.pathname;

        switch(hostname) {
            case "github.com":
                handleGitHub(pathname);
                break;
            case "gitlab.com":
                handleGitLab(pathname);
                break;
            case "bitbucket.org":
                handleBitbucket(pathname);
                break;
            default:
              alert("No adequate hostname!")
        }
    });
}

async function handleGitHub(pathname){
    platformId = 1;
    userName = pathname.split("/")[1];

    userUrl = 'https://api.github.com/users/' + userName;

    await fetch(userUrl).then(function(response) {
        if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
        }
        return response.json();
    }).then((json) => {
       userId = json['id'];
      });
    
    fetchRepos(approach, platformId, userId, userRatings, cold=false);
}

async function handleGitLab(pathname){
    platformId = 2;
    userName = pathname.split("/")[1];

    userUrl = 'https://gitlab.com/api/v4/users?username=' + userName;

    await fetch(userUrl).then(function(response) {
        if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
        }
        return response.json();
    }).then((json) => {
       userId = json[0]['id'];
      });
    
    fetchRepos(approach, platformId, userId, userRatings, cold=false);
}

async function handleBitbucket(pathname){
    platformId = 3;
    userId = pathname.split("/")[1];
    userUrl = 'https://api.bitbucket.org/2.0/users/' + userId;
    userId = userId.replace('%7B', '');
    userId = userId.replace('%7D', '');

    await fetch(userUrl).then(function(response) {
        if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
        }
        return response.json();
    }).then((json) => {
       userName = json['display_name'];
    });
    
    fetchRepos(approach, platformId, userId, userRatings, cold=false);
}

async function fetchRepos(approach, platformId, userId, userRatings, cold){
    let fetchUrl = '';

    if (cold === true){
        fetchUrl = 'http://di-repository.uni-muenster.de/api/' + approach + '?user_ratings=' + JSON.stringify(userRatings)
    } else {
        fetchUrl = 'http://di-repository.uni-muenster.de/api/' + approach + '?platform_id=' + platformId + '&user_id=' + userId
    }

    fetch(fetchUrl).then(function(response) {
        let json = response.clone().json()
        json.then(json => {
			if ("message" in json) {
				fetchTopics().then(topics => {
                createTopicRatings(topics)
				})
			}
		})
		return response.json()
    }).then(function(json) {
        if (json.length > 0) {

            let h3 = document.createElement("h3")
            h3.id = "repoH3"
            h3.innerHTML = "Repommender"

            let welcomeText = document.createElement("p")
            welcomeText.innerHTML = "Welcome, <b>" + userName + "</b>! Here are coding repositories we recommended you " +
            "based on your personal interests."
            welcomeText.className = "text-secondary"

            if (approach === "cf") {
                approachLong = 'Collaborative Filtering'
            } else {
                approachLong = 'Content Based'
            }

            let approachDiv = document.createElement("div")
            approachDiv.className="p-2 d-flex flex-column flex-wrap justify-content-between align-items-baseline"

            let approachText = document.createElement("p")
            approachText.className = "text-secondary"
            approachText.innerHTML = "Chosen Recommendation approach: " + '<b>' +  approachLong + '</b>'

            let toggleApproach = document.createElement("button")
            toggleApproach.className = "btn btn-dark"
            toggleApproach.innerHTML = "Toggle Recommendation Approach"
            toggleApproach.addEventListener("click", function(){
                document.getElementById("repo-container").innerHTML = '';
                if (approach === 'cf'){
                    approach = 'cb'
                } else {
                    approach = 'cf'
                }
                if (userRatings.length === 0){
                    fetchRepos(approach, platformId, userId, userRatings, cold=false)
                } else {
                    fetchRepos(approach, platformId = null, userId = null, userRatings, cold=true)
                }
            })
            approachDiv.appendChild(approachText)
            approachDiv.appendChild(toggleApproach)

            document.getElementById("repo-container").appendChild(h3)
            document.getElementById("repo-container").appendChild(welcomeText)
            document.getElementById("repo-container").appendChild(approachDiv);


            json.forEach(function(repo) {
                let imgSrc = ''
                switch(repo.platform_id) {
                    case 1:
                        imgSrc = 'images/github.png'
                        break;
                    case 2:
                        imgSrc = 'images/gitlab.png'
                        break;
                    case 3:
                        imgSrc = 'images/bitbucket.png'
                        break;
                }

                var repoDiv = document.createElement("div");
                repoDiv.className = "repos p-2";             
    
                var card = document.createElement("div");
                card.className = "card";
    
                var cardBody = document.createElement("div");
                cardBody.className = "card-body";
                cardBody.id = "cardBody";
                cardBody.addEventListener('click', function() {
                    chrome.tabs.create({ url: repo.link });
                });
    
                var headerDiv = document.createElement("div");
                headerDiv.className = "d-flex flex-column flex-wrap align-items-baseline";

                let titleDiv = document.createElement("div")
                titleDiv.className = "d-flex flex-row flex-wrap justify-content-between align-items-center";
                titleDiv.id = "titleDiv"
    
                var title = document.createElement("h4");
                title.innerHTML = repo.name;
                title.className = "card-title";

                var platformLogo = document.createElement("img")
                platformLogo.src = imgSrc
                platformLogo.style.backgroundColor = 'transparent'

                titleDiv.appendChild(title)
                titleDiv.appendChild(platformLogo)
    
                var idOwnerDiv = document.createElement("div");
                idOwnerDiv.className = "d-flex flex-wrap";
    
                var id = document.createElement("span");
                id.innerHTML = "Project-ID: " + repo.id;
                id.className = "text-secondary project-id"
    
                var owner = document.createElement("span");
                if (repo.owner_name != null) {
                    owner.innerHTML = "Owner: " + repo.owner_name;
                } else {
                    owner.innerHTML = "No owner information available.";
                }
                owner.className = "owner text-secondary";
    
                idOwnerDiv.appendChild(id);
                idOwnerDiv.appendChild(owner);
    
                headerDiv.appendChild(titleDiv);
                headerDiv.appendChild(idOwnerDiv);
    
                var desc = document.createElement("p");
                if (repo.description !== "") {
                    desc.innerHTML = repo.description;
                } else {
                    desc.innerHTML = "No description available.";
                }
    
                var language = document.createElement("p");
                language.innerHTML = "<b>Language: </b>" + repo.language;
    
                var dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: '2-digit' };
    
                var created = document.createElement("p");
                const createdDate = new Date(repo.created + 'Z');
                created.innerHTML = "<b>Created: </b>" + createdDate.toLocaleDateString("en-US", dateOptions);
    
                var lastUpdated = document.createElement("p");
                const lastUpdatedDate = new Date(repo.last_updated + 'Z');
                lastUpdated.innerHTML = "<b>Last updated: </b>" + lastUpdatedDate.toLocaleDateString("en-US", dateOptions);
    
                cardBody.appendChild(headerDiv);
                cardBody.appendChild(desc);
                if (repo.language !== null) {
                    cardBody.appendChild(language);
                }
                cardBody.appendChild(created);
                cardBody.appendChild(lastUpdated);
    
                var scoreContainer = document.createElement("div");
				scoreContainer.className = "card-footer d-flex flex-row flex-wrap justify-content-center align-items-baseline";
                
				let scoreTitle = document.createElement("b")
				scoreTitle.innerHTML = "Score: "
				scoreTitle.id = "scoreTitle"

				let score = document.createElement("span")
				score.innerHTML = Math.round(( (repo.score * 100) + Number.EPSILON) * 100) / 100 + '%' ;
				score.id = "score"
	
				scoreContainer.appendChild(scoreTitle)
				scoreContainer.appendChild(score)
	
                card.appendChild(cardBody);
                card.appendChild(scoreContainer);
    
                repoDiv.appendChild(card);
    
                document.getElementById("repo-container").appendChild(repoDiv);
            })
        }
    })
}

async function fetchTopics(){
    await fetch('http://di-repository.uni-muenster.de/api/topics').then(response =>{
        if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
        }
        return response.json();
    }).then(json => {
        topics = json
        topics.forEach(topic => {
            ratings.push({
                topic: topic.name,
                rating: 1
            })
            userRatings.push({
                id: topic.id,
                topic: topic.name,
                rating: 1
            })
        })
    })
    return topics;
}

async function createTopicRatings(topics){

    let h3 = document.createElement("h3")
    h3.id = "repoH3"
    h3.innerHTML = "Repommender"

    let welcomeText = document.createElement("p")
    welcomeText.innerHTML = "Welcome, <b>" + userName + "</b>! Here are 10 example topics drawn from the most used topics in Coding Repositories. "
    + "Please rate them from 1-5 and click 'Send' at the bottom of this window. Repommender will then provide repository recommendations "
    + "based on your ratings."
    welcomeText.className = "text-secondary"

    document.getElementById("repo-container").appendChild(h3)
    document.getElementById("repo-container").appendChild(welcomeText);

    
    topics.forEach(topic => {

        let topicContainer = document.createElement("div")
        topicContainer.className = "p-2"

        let topicDiv = document.createElement("div");
        topicDiv.className = "card d-flex flex-row flex-wrap align-items-baseline";

        let dropdownDiv = document.createElement("div")
        dropdownDiv.className = "dropdown d-flex flex-row flex-wrap justify-content-between align-items-baseline"

        let topicName = document.createElement("div")
        topicName.className = "topicName"
        topicName.innerHTML = topic.name

        let dropdownButton = document.createElement("button")
        dropdownButton.id = "dropbtn"
        dropdownButton.className = "btn btn-dark"
        dropdownButton.innerHTML = "Rate topic"
        dropdownButton.addEventListener("click",function(){
            dropdownMenuDiv.classList.toggle("show");
        })

        let dropdownMenuDiv = document.createElement("div")
        dropdownMenuDiv.className = "dropdown-content"
        dropdownMenuDiv.id = "myDropdown"


        let star1 = document.createElement("a")
        star1.innerHTML = 1
        star1.addEventListener("click", function(){
            let rlength = ratings.length
            let uRlength = userRatings.length
            let index = ratings.findIndex(element => element.topic === topic.name)
            ratings[index].rating = 1
            sum = 0;
            for(let i=0; i < rlength; i++){
                sum = sum + ratings[i].rating
            }
            for(let i=0; i < uRlength; i++){
                userRatings[i].rating = Math.round(( (ratings[i].rating/sum) + Number.EPSILON) * 100) / 100
            }
            ratingDiv.innerHTML = ratings[index].rating + " / 5"
        })

        let star2 = document.createElement("a")
        star2.innerHTML = 2
        star2.addEventListener("click", function(){
            let rlength = ratings.length
            let uRlength = userRatings.length
            let index = ratings.findIndex(element => element.topic === topic.name);
            ratings[index].rating = 2;
            sum = 0;
            for(let i=0; i < rlength; i++){
                sum = sum + ratings[i].rating
            }
            for(let i=0; i < uRlength; i++){
                userRatings[i].rating = Math.round(( (ratings[i].rating/sum) + Number.EPSILON) * 100) / 100
            }
            ratingDiv.innerHTML = ratings[index].rating + " / 5"
        })

        let star3 = document.createElement("a")
        star3.innerHTML = 3
        star3.addEventListener("click", function(){
            let rlength = ratings.length
            let uRlength = userRatings.length
            let index = ratings.findIndex(element => element.topic === topic.name);
            ratings[index].rating = 3;
            sum = 0;
            for(let i=0; i < rlength; i++){
                sum = sum + ratings[i].rating
            }
            for(let i=0; i < uRlength; i++){
                userRatings[i].rating = Math.round(( (ratings[i].rating/sum) + Number.EPSILON) * 100) / 100
            }
            ratingDiv.innerHTML = ratings[index].rating + " / 5"
        })

        let star4 = document.createElement("a")
        star4.innerHTML = 4
        star4.addEventListener("click", function(){
            let rlength = ratings.length
            let uRlength = userRatings.length
            let index = ratings.findIndex(element => element.topic === topic.name);
            ratings[index].rating = 4;
            sum = 0;
            for(let i=0; i < rlength; i++){
                sum = sum + ratings[i].rating
            }
            for(let i=0; i < uRlength; i++){
                userRatings[i].rating = Math.round(( (ratings[i].rating/sum) + Number.EPSILON) * 100) / 100
            }
            ratingDiv.innerHTML = ratings[index].rating + " / 5"
        })

        let star5 = document.createElement("a")
        star5.innerHTML = 5
        star5.addEventListener("click", function(){
            let rlength = ratings.length
            let uRlength = userRatings.length
            let index = ratings.findIndex(element => element.topic === topic.name);
            ratings[index].rating = 5;
            sum = 0;
            for(let i=0; i < rlength; i++){
                sum = sum + ratings[i].rating
            }
            for(let i=0; i < uRlength; i++){
                userRatings[i].rating = Math.round(( (ratings[i].rating/sum) + Number.EPSILON) * 100) / 100
            }
            ratingDiv.innerHTML = ratings[index].rating + " / 5"
        })
        
        let ratingDiv = document.createElement("div")
        ratingDiv.id = "ratingDiv"
        ratingDiv.innerHTML = "1 / 5"

        dropdownMenuDiv.appendChild(star1)
        dropdownMenuDiv.appendChild(star2)
        dropdownMenuDiv.appendChild(star3)
        dropdownMenuDiv.appendChild(star4)
        dropdownMenuDiv.appendChild(star5)

        dropdownDiv.appendChild(topicName)
        dropdownDiv.appendChild(dropdownButton)
        dropdownDiv.appendChild(dropdownMenuDiv)
        dropdownDiv.appendChild(ratingDiv)

        topicDiv.appendChild(dropdownDiv)

        topicContainer.appendChild(topicDiv)

        document.getElementById("repo-container").appendChild(topicContainer);
    })

    let sendButton = document.createElement("button")
    sendButton.id = "sendBtn"
    sendButton.className = "btn btn-dark"
    sendButton.innerHTML = "Send Ratings"
    sendButton.addEventListener("click", function(){
        document.getElementById("repo-container").innerHTML = '';
        fetchRepos(approach, platformId = null, userId = null, userRatings, cold=true)
    })

    document.getElementById("repo-container").appendChild(sendButton);
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('#dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}
